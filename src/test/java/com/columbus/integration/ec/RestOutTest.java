package com.columbus.integration.ec;

import com.columbus.integration.ec.communication.RestOut;
import com.google.common.io.Files;
import com.intentia.ec.shared.Manifest;
import com.intentia.ec.shared.MessageSenderException;
import org.apache.http.Header;
import org.apache.http.HttpEntity;
import org.apache.http.HttpStatus;
import org.apache.http.StatusLine;
import org.apache.http.client.methods.CloseableHttpResponse;
import org.apache.http.client.methods.HttpUriRequest;
import org.apache.http.impl.client.CloseableHttpClient;
import org.junit.Before;
import org.junit.Test;
import org.mockito.Mock;

import java.io.File;
import java.io.InputStream;
import java.util.Properties;

import static org.junit.Assert.fail;
import static org.mockito.Matchers.any;
import static org.mockito.Mockito.anyString;
import static org.mockito.Mockito.when;
import static org.mockito.MockitoAnnotations.initMocks;

public class RestOutTest {
    private final static String WWW_AUTHENTICATE_HEADER_NAME = "WWW-Authenticate";
    private final static String ERROR_DESCRIPTION_HEADER_VALUE = "Bearer realm=\"{realm specific to the platform}\", error= \"invalid_token\", error_description=\"Invalid token signature\"";

    private RestOut restOut;
    private InputStream inputStream;
    @Mock
    CloseableHttpClient closeableHttpClient;
    @Mock
    Manifest manifest;
    @Mock
    Properties properties;
    @Mock
    private CloseableHttpResponse postResponse;
    @Mock
    private CloseableHttpResponse tokenResponse;
    @Mock
    private HttpEntity responseEntity;
    @Mock
    private InputStream errorResponseBody;
    @Mock
    private StatusLine statusLine;
    @Mock
    private Header errorDescriptionHeader;

    @Before
    public void setUp() {
        initMocks(this);
        restOut = new RestOut(closeableHttpClient);
    }

    @Test(expected = MessageSenderException.class)
    public void shouldThrowExceptionOnHttp400Response() throws Exception {
        // Given
        givenUrl();
        givenInputStream();
        givenHttpStatus400();
        // When
        restOut.send(inputStream, manifest, properties);
        // Then
        fail();
    }

    @Test (expected = MessageSenderException.class)
    public void shouldFailWhenTokenRenewalWasUnsuccessful() throws Exception {
        // Given
        givenUrl();
        givenInputStream();
        givenHttpStatus401();
        when(statusLine.getStatusCode()).thenReturn(
                HttpStatus.SC_UNAUTHORIZED,     // First response for post in handleResponse if OK
                HttpStatus.SC_UNAUTHORIZED,     // First response for post in handleResponse if Unauthorized
                HttpStatus.SC_BAD_REQUEST       // Second response for token request in handleTokenResponse if OK
        );
        // When
        restOut.send(inputStream, manifest, properties);
        // Then
        fail();

    }

    private void givenUrl() {
        when(properties.getProperty("Host")).thenReturn("https://example.com/rest");
    }

    private void givenInputStream() throws Exception {
        inputStream = Files.asByteSource(new File("src/test/resources/ttListOfEvents.xml")).openStream();
    }

    private void givenHttpStatus200() throws Exception {
        when(closeableHttpClient.execute(any(HttpUriRequest.class))).thenReturn(postResponse);
        when(postResponse.getStatusLine()).thenReturn(statusLine);
        when(statusLine.getStatusCode()).thenReturn(HttpStatus.SC_OK);
        when(statusLine.getReasonPhrase()).thenReturn("OK");
    }

    private void givenHttpStatus400() throws Exception {
        errorResponseBody = Files.asByteSource(new File("src/test/resources/errorMessageBody.xml")).openStream();
        when(closeableHttpClient.execute(any(HttpUriRequest.class))).thenReturn(postResponse);
        when(postResponse.getStatusLine()).thenReturn(statusLine);
        when(statusLine.getStatusCode()).thenReturn(HttpStatus.SC_BAD_REQUEST);
        when(statusLine.getReasonPhrase()).thenReturn("Bad request");
        when(postResponse.getEntity()).thenReturn(responseEntity);
        when(responseEntity.getContent()).thenReturn(errorResponseBody);
        when(postResponse.getAllHeaders()).thenReturn(new Header[]{});
    }

    private void givenHttpStatus401() throws Exception {
        InputStream tokenResponseBody = Files.asByteSource(new File("src/test/resources/token.json")).openStream();
        when(closeableHttpClient.execute(any(HttpUriRequest.class))).thenReturn(postResponse, tokenResponse, postResponse);
        when(postResponse.getStatusLine()).thenReturn(statusLine);

        when(statusLine.getReasonPhrase()).thenReturn("OK");
        when(postResponse.containsHeader(WWW_AUTHENTICATE_HEADER_NAME)).thenReturn(true);
        when(postResponse.getFirstHeader(anyString())).thenReturn(errorDescriptionHeader);
        when(errorDescriptionHeader.getValue()).thenReturn(ERROR_DESCRIPTION_HEADER_VALUE);
        when(tokenResponse.getStatusLine()).thenReturn(statusLine);
        when(tokenResponse.getEntity()).thenReturn(responseEntity);
        when(responseEntity.getContent()).thenReturn(tokenResponseBody);
    }

    // Not a unit test, sends data over http to repository
    @Test
    public void sendEpcisDocument() throws Exception {
        Properties properties = new Properties();
        properties.setProperty("Protocol", "https://");
        properties.setProperty("Content-Type", "application/xml");
        properties.setProperty("Host", "cm.qlf-mb-private.tpd.as8677.net");
        properties.setProperty("Endpoint", "/v1/cm/epcis");
        properties.setProperty("Expect", "100-continue");
        properties.setProperty("Connection", "Keep-Alive");
        properties.setProperty("accessTokenUrl", getAccessTokenUrl());
        properties.setProperty("client_id", getClientId());
        properties.setProperty("client_secret", getClientSecret());
        properties.setProperty("grant_type", getGrantType());
        properties.setProperty("accessTokenHeaders", getAccessTokenHeaders());
        RestOut realRestOut = new RestOut();

        givenInputStream();

        realRestOut.send(inputStream, manifest, properties);
    }

    private String getAccessTokenUrl() {
        return "https://auth-qlf.caas.tpd-pmp.as8677.net/auth/realms/mb_qualif_private/protocol/openid-connect/token";
    }
    private String getClientId() {
        return "mb_istone";
    }

    private String getClientSecret() {
        return "85c576a8-2278-4451-a29e-1d566b49edf1"/*"e726114a-420f-4fe4-a85f-0c8c50fe7001"*/;
    }

    private String getGrantType() {
        return "client_credentials";
    }

    private String getAccessTokenHeaders() {
        return "Cache-Control,no-cache;Content-Type,application/x-www-form-urlencoded;Host,auth-qlf.caas.tpd-pmp.as8677.net";
    }
}
