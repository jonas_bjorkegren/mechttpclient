package com.columbus.integration.ec.communication;

import com.google.common.annotations.VisibleForTesting;
import com.google.common.base.Charsets;
import com.google.common.base.Joiner;
import com.google.common.io.CharStreams;
import com.intentia.ec.shared.IMessageSender;
import com.intentia.ec.shared.Manifest;
import com.intentia.ec.shared.MessageSenderException;
import org.apache.http.Header;
import org.apache.http.HttpStatus;
import org.apache.http.client.methods.CloseableHttpResponse;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.client.methods.HttpRequestBase;
import org.apache.http.entity.ContentType;
import org.apache.http.entity.StringEntity;
import org.apache.http.impl.client.CloseableHttpClient;
import org.apache.http.impl.client.HttpClients;
import org.apache.log4j.Category;
import org.json.JSONObject;

import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.util.Properties;

public class RestOut implements IMessageSender {
    private final static String ACCESS_TOKEN_URL= "accessTokenUrl";
    private final static String ACCESS_TOKEN_ATTRIBUTE_NAME = "access_token";
    private final static String AUTHORIZATION_HEADER_NAME = "Authorization";
    private final static String AUTHORIZATION_CLIENT_ID = "client_id";
    private final static String AUTHORIZATION_CLIENT_SECRET = "client_secret";
    private final static String AUTHORIZATION_GRANT_TYPE = "grant_type";
    private final static String ACCESS_TOKEN_HEADERS = "accessTokenHeaders";

    static Category cat = Category.getInstance(RestOut.class.getName());
    private CloseableHttpClient httpClient;

    public RestOut() {
        httpClient  = HttpClients.custom().build();
    }

    @VisibleForTesting
    public RestOut(CloseableHttpClient httpClient) {
        this.httpClient = httpClient;
    }

    public void send(InputStream inputStream, Manifest manifest, Properties properties) throws MessageSenderException {
        HttpPost post = new HttpPost(properties.getProperty("Protocol") + properties.getProperty("Host") + properties.getProperty("Endpoint"));

        if (cat.isDebugEnabled()) {
            cat.debug(manifest.getUUID() + ":" + "Request url:");
            cat.debug(manifest.getUUID() + ":" + properties.getProperty("Protocol") + properties.getProperty("Host") + properties.getProperty("Endpoint"));
            cat.debug("Request headers:");
            for (Header header : post.getAllHeaders()) {
                cat.debug(manifest.getUUID() + ":" + header.getName() + ": " + header.getValue());
            }
        }

        try {
            // For every request we always ask for a new access token
            String accessToken = getAccessToken(properties);
            // We set the headers for our business request
            setHeaders(post, properties, accessToken);
            // We set the body for our business request
            setBody(post, inputStream);
            // We execute our business request and handle the response (if we received a http status other than 200 we raise an error)
            handleResponse(httpClient.execute(post));
        } catch (MessageSenderException e) {
            throw e;
        } catch (Exception e) {
            throw new MessageSenderException("Error while reading or writing message " , e);
        }
    }

    private void setHeaders(HttpRequestBase httpRequestBase, Properties properties, String accessToken) {
        httpRequestBase.setHeader(AUTHORIZATION_HEADER_NAME, "Bearer " + accessToken);
        httpRequestBase.setHeader("Content-Type", properties.getProperty("Content-Type"));
        httpRequestBase.setHeader("Host", properties.getProperty("Host"));
        httpRequestBase.setHeader("Expect", properties.getProperty("Expect"));
        httpRequestBase.setHeader("Connection", properties.getProperty("Connection"));
    }

    private void setBody(HttpPost post, InputStream inputStream) throws MessageSenderException {
        try {
            String body = CharStreams.toString(new InputStreamReader(inputStream, Charsets.UTF_8));
            post.setEntity(new StringEntity(body, ContentType.create("text/xml", "utf-8")));
        } catch (IOException e) {
            throw new MessageSenderException(e);
        }
    }

    private void handleResponse(CloseableHttpResponse response) throws MessageSenderException {
        if (response.getStatusLine().getStatusCode() == HttpStatus.SC_OK) {
            if (cat.isDebugEnabled()) {
                String message = Joiner.on(" ").join("External service responded with",
                        response.getStatusLine().getStatusCode(), " - ", response.getStatusLine().getReasonPhrase());
                cat.debug(message);
            }
            printResponse(response);
        } else {
            String message = Joiner.on(" ").join("External service responded with error:",
                    response.getStatusLine().getStatusCode(), "-", response.getStatusLine().getReasonPhrase());
            if (cat.isDebugEnabled()) {
                cat.debug("Response headers:");
                for (Header header : response.getAllHeaders()) {
                    cat.debug(header.getName() + " - " + header.getValue());
                }
                try {
                    cat.debug("Response body:");
                    cat.debug(CharStreams.toString(new InputStreamReader(response.getEntity().getContent(), Charsets.UTF_8)));
                } catch (IOException e) {
                    throw new MessageSenderException(e);
                }
            }
            throw new MessageSenderException(message);
        }
    }

    private String getAccessToken(Properties properties) throws MessageSenderException {
        String accessToken;
        HttpPost accessTokenPost = new HttpPost(properties.getProperty(ACCESS_TOKEN_URL));

        setAccessTokenRequestHeaders(accessTokenPost, properties.getProperty(ACCESS_TOKEN_HEADERS));
        setAccessTokenRequestBody(accessTokenPost, properties.getProperty(AUTHORIZATION_GRANT_TYPE),
                properties.getProperty(AUTHORIZATION_CLIENT_ID), properties.getProperty(AUTHORIZATION_CLIENT_SECRET));

        try {
            accessToken = handleAccessTokenResponse(httpClient.execute(accessTokenPost));
        } catch (Exception e) {
            throw new MessageSenderException(e);
        }

        return accessToken;
    }

    private void setAccessTokenRequestHeaders(HttpRequestBase post, String accessTokenHeaders) {
        // accessTokenHeaders is a name value semicolon separated String, where name and value are separated by comma
        // e.g name,value;name,value ...
        String [] headers = accessTokenHeaders.split(";");
        for (String header : headers) {
            String [] nameValuePair = header.split(",");
            post.setHeader(nameValuePair[0], nameValuePair[1]);
        }
    }

    private void setAccessTokenRequestBody(HttpPost post, String grantType, String clientId, String secret) {
        post.setEntity(new StringEntity(
                "grant_type=" + grantType + "&client_id=" + clientId + "&client_secret=" + secret, ContentType.create("text/xml", "utf-8")));
    }

    private String handleAccessTokenResponse(CloseableHttpResponse response) throws Exception {
        String accessToken;
        if (response.getStatusLine().getStatusCode() == HttpStatus.SC_OK) {
            JSONObject jsonResponse   = new JSONObject(CharStreams.toString(new InputStreamReader(response.getEntity().getContent(), Charsets.UTF_8)));
            //System.out.println(jsonResponse.toString());
            accessToken = jsonResponse.getString(ACCESS_TOKEN_ATTRIBUTE_NAME);
            //System.out.println("Access token last 10 characters: " + accessToken.substring(accessToken.length() - 10));
        } else {
            String body = "";
            try {
                body = CharStreams.toString(new InputStreamReader(response.getEntity().getContent(), Charsets.UTF_8));
            } catch (IOException e) {
                // do nothing
            }
            throw new RuntimeException(Joiner.on(" ").join("Access token renewal service responded with", response.getStatusLine().getStatusCode(), "-",
                    response.getStatusLine().getReasonPhrase(), " - ", body));
        }
        return accessToken;
    }

    // Use locally for testing
    private void printResponse(CloseableHttpResponse response) {
        System.out.println(response.getStatusLine().getStatusCode() + " : " + response.getStatusLine().getReasonPhrase());
        for (Header header : response.getAllHeaders()) {
            System.out.println(header.getName() + ": " + header.getValue());
        }
        try {
            String  body = CharStreams.toString(new InputStreamReader(response.getEntity().getContent(), Charsets.UTF_8));
            System.out.println(body);
        } catch (IOException e) {
            System.out.println(e.getMessage());
        }
    }
}
