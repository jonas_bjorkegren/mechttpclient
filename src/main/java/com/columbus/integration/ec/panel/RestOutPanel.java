package com.columbus.integration.ec.panel;

import com.intentia.ec.partneradmin.swt.GUIHelpers;
import com.intentia.ec.shared.IRoutingUI;
import org.eclipse.swt.events.SelectionEvent;
import org.eclipse.swt.events.SelectionListener;
import org.eclipse.swt.layout.GridData;
import org.eclipse.swt.layout.GridLayout;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.swt.widgets.Group;
import org.eclipse.swt.widgets.Label;
import org.eclipse.swt.widgets.Text;

import java.util.Properties;

public class RestOutPanel implements IRoutingUI, SelectionListener {
    private Text txtHost;
    private Text txtProtocol;
    private Text txtEndpoint;
    private Text txtExpect;
    private Text txtConnection;
    private Text txtContentType;

    private Text txtAccessTokenUrl;
    private Text txtClientId;
    private Text txtClientSecret;
    private Text txtGrantType;
    private Text txtAccessTokenHeaders;

    private Composite composite;

    public void setProperties(Properties properties) {
        if (properties != null) {
            this.txtHost.setText(properties.getProperty("Host", ""));
            this.txtProtocol.setText(properties.getProperty("Protocol", ""));
            this.txtEndpoint.setText(properties.getProperty("Endpoint", ""));
            this.txtContentType.setText(properties.getProperty("Content-Type", "text/xml"));
            this.txtExpect.setText(properties.getProperty("Expect", ""));
            this.txtConnection.setText(properties.getProperty("Connection", ""));

            this.txtAccessTokenUrl.setText(properties.getProperty("accessTokenUrl", ""));
            this.txtClientId.setText(properties.getProperty("client_id", ""));
            this.txtClientSecret.setText(properties.getProperty("client_secret", ""));
            this.txtGrantType.setText(properties.getProperty("grant_type", ""));
            this.txtAccessTokenHeaders.setText(properties.getProperty("accessTokenHeaders", ""));
        }
    }

    public Properties getProperties() {
        Properties properties = new Properties();
        if (!this.checkFields()) {
            return null;
        } else {
            properties.setProperty("Host", this.txtHost.getText().trim());
            properties.setProperty("Protocol", this.txtProtocol.getText().trim());
            properties.setProperty("Endpoint", this.txtEndpoint.getText().trim());
            properties.setProperty("Content-Type", this.txtContentType.getText().trim());
            properties.setProperty("Expect", this.txtExpect.getText().trim());
            properties.setProperty("Connection", this.txtConnection.getText().trim());

            properties.setProperty("accessTokenUrl", this.txtAccessTokenUrl.getText().trim());
            properties.setProperty("client_id", this.txtClientId.getText().trim());
            properties.setProperty("client_secret", this.txtClientSecret.getText().trim());
            properties.setProperty("grant_type", this.txtGrantType.getText().trim());
            properties.setProperty("accessTokenHeaders", this.txtAccessTokenHeaders.getText().trim());

            return properties;
        }
    }

    public boolean checkFields() {
        if (this.txtHost.getText().length() == 0) {
            GUIHelpers.openError(this.composite.getShell(), "Invalid value", "Host cannot be empty.");
            return false;
        } else if (this.txtAccessTokenUrl.getText().length() == 0) {
            GUIHelpers.openError(this.composite.getShell(), "Invalid value", "Access token url cannot be empty.");
            return false;
        } else {
            try {
                if (this.txtProtocol.getText().isEmpty() || this.txtProtocol.getText() == null) {
                    throw new RuntimeException();
                } else {
                    return true;
                }
            } catch (RuntimeException var2) {
                GUIHelpers.openError(this.composite.getShell(), "Invalid value", "Protocol needs to be defined.");
                return false;
            }
        }
    }

    public void createContent(Composite parent) {
        this.composite = new Composite(parent, 0);
        GridLayout gl = new GridLayout(1, false);
        gl.verticalSpacing = 10;
        gl.marginHeight = 0;
        gl.marginWidth = 0;
        this.composite.setLayout(gl);
        GridData data = new GridData(768);
        GridLayout grpBasicLayout = new GridLayout(2, false);
        grpBasicLayout.verticalSpacing = 10;
        Group basicGrp = new Group(this.composite, 0);
        basicGrp.setText("Basic configuration");
        basicGrp.setLayoutData(data);
        basicGrp.setLayout(grpBasicLayout);

        (new Label(basicGrp, 0)).setText("Host:");
        data = new GridData();
        data.horizontalAlignment = 4;
        data.grabExcessHorizontalSpace = true;
        this.txtHost = new Text(basicGrp, 2048);
        this.txtHost.setLayoutData(data);

        (new Label(basicGrp, 0)).setText("Protocol:");
        data = new GridData();
        data.horizontalAlignment = 4;
        data.grabExcessHorizontalSpace = true;
        this.txtProtocol = new Text(basicGrp, 2048);
        this.txtProtocol.setLayoutData(data);

        (new Label(basicGrp, 0)).setText("Endpoint:");
        data = new GridData();
        data.horizontalAlignment = 4;
        data.grabExcessHorizontalSpace = true;
        this.txtEndpoint = new Text(basicGrp, 2048);

        (new Label(basicGrp, 0)).setText("Content type:");
        data = new GridData();
        data.horizontalAlignment = 4;
        data.grabExcessHorizontalSpace = true;
        this.txtContentType = new Text(basicGrp, 2048);
        this.txtContentType.setLayoutData(data);

        (new Label(basicGrp, 0)).setText("Expect:");
        data = new GridData();
        data.horizontalAlignment = 4;
        data.grabExcessHorizontalSpace = true;
        this.txtExpect = new Text(basicGrp, 2048);
        this.txtExpect.setLayoutData(data);

        (new Label(basicGrp, 0)).setText("Connection:");
        data = new GridData();
        data.horizontalAlignment = 4;
        data.grabExcessHorizontalSpace = true;
        this.txtConnection = new Text(basicGrp, 2048);
        this.txtConnection.setLayoutData(data);

        (new Label(basicGrp, 0)).setText("Access token url):");
        data = new GridData();
        data.horizontalAlignment = 4;
        data.grabExcessHorizontalSpace = true;
        this.txtAccessTokenUrl = new Text(basicGrp, 2048);
        this.txtAccessTokenUrl.setLayoutData(data);

        (new Label(basicGrp, 0)).setText("Client id:");
        data = new GridData();
        data.horizontalAlignment = 4;
        data.grabExcessHorizontalSpace = true;
        this.txtClientId = new Text(basicGrp, 2048);
        this.txtClientId.setLayoutData(data);

        (new Label(basicGrp, 0)).setText("Client secret:");
        data = new GridData();
        data.horizontalAlignment = 4;
        data.grabExcessHorizontalSpace = true;
        this.txtClientSecret = new Text(basicGrp, 2048);
        this.txtClientSecret.setLayoutData(data);

        (new Label(basicGrp, 0)).setText("Grant type:");
        data = new GridData();
        data.horizontalAlignment = 4;
        data.grabExcessHorizontalSpace = true;
        this.txtGrantType = new Text(basicGrp, 2048);
        this.txtGrantType.setLayoutData(data);

        (new Label(basicGrp, 0)).setText("Access token headers (n,v;n,v;n,v ...");
        data = new GridData();
        data.horizontalAlignment = 4;
        data.grabExcessHorizontalSpace = true;
        this.txtAccessTokenHeaders = new Text(basicGrp, 2048);
        this.txtAccessTokenHeaders.setLayoutData(data);
    }

    public void widgetSelected(SelectionEvent selectionEvent) {

    }

    public void widgetDefaultSelected(SelectionEvent selectionEvent) {

    }
}
